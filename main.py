'''
Date: 2021-12-12 10:22:53
LastEditors: will.chengyong
LastEditTime: 2021-12-14 10:20:59
FilePath: /general/christmas/main.py
'''

from invitation import Invitation, ImageUtils
from content import create_content

def generate_card(
    bg_image_file: str, 
    fonts_dir: str,
    card_recipient: str,
    gift_recipient: str,
    ) -> ImageUtils:
    invitation = Invitation(bg_image_file, fonts_dir)
    content = create_content(card_recipient, gift_recipient)

    image = None
    for c in content:
        image = invitation.put_text(
            c.get("text"), 
            c.get("font_size"),
            c.get("thickness"),
            color=c.get("color"), 
            distance_percentage_from_top=c.get("margin_top"))
    return image

def main() -> None:
    bg_image_file  = "assets/xmas-invitation-bg.jpg"
    fonts_dir      = "fonts"
    card_recipient = "Willmermaid"
    gift_recipient = "玉澤演"
    save_file      = "assets/xmas-invitation.jpg"

    image = generate_card(bg_image_file, fonts_dir, 
                            card_recipient, gift_recipient)
    image.save(save_file)

if __name__ == "__main__":
    main()