'''
Date: 2021-12-12 15:58:19
LastEditors: will.chengyong
LastEditTime: 2021-12-14 10:20:17
FilePath: /general/christmas/content.py
'''

from invitation import Thickness

def create_content(card_recipient: str, gift_recipient: str) -> dict:
    return [
        {
            "text": "please join us for a".upper(),
            "color": (255, 255, 255),
            "font_size": 25,
            "thickness": Thickness.MEDIUM,
            "margin_top": 0.18
        },
        {
            "text": "christmas party".upper(),
            "color": (222, 205, 137),
            "font_size": 50,
            "thickness": Thickness.BOLD,
            "margin_top": 0.23
        },
        {
            "text": "-" * 50,
            "color": (56, 67, 115),
            "font_size": 22,
            "thickness": Thickness.REGULAR,
            "margin_top": 0.68
        },
        {
            "text": "2021.12.25 13:00".upper(),
            "color": (188, 188, 188),
            "font_size": 30,
            "thickness": Thickness.REGULAR,
            "margin_top": 0.73
        },
        {
            "text": "格鲁·秀色 Georgia’s Feast",
            "color": (188, 188, 188),
            "font_size": 22,
            "thickness": Thickness.REGULAR,
            "margin_top": 0.79
        },
        {
            "text": "北京市東城區東直門內大街9號院",
            "color": (188, 188, 188),
            "font_size": 19,
            "thickness": Thickness.REGULAR,
            "margin_top": 0.825
        },
        {
            "text": "親愛的 {}".format(card_recipient),
            "color": (222, 205, 137),
            "font_size": 30,
            "thickness": Thickness.BOLD,
            "margin_top": 0.35
        },
        {
            "text": "歡迎參加 2021 年度的聖誕聚會",
            "color": (222, 205, 137),
            "font_size": 25,
            "thickness": Thickness.MEDIUM,
            "margin_top": 0.41
        },
        {
            "text": "你的送禮對象是 {}".format(gift_recipient),
            "color": (222, 205, 137),
            "font_size": 25,
            "thickness": Thickness.MEDIUM,
            "margin_top": 0.45
        }
    ]