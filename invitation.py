'''
Date: 2021-12-12 10:23:23
LastEditors: will.chengyong
LastEditTime: 2021-12-12 16:01:22
FilePath: /general/christmas/invitation.py
'''

import os
import logging
from enum import Enum
from copy import deepcopy

import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont

logging.basicConfig(level=logging.INFO)

class Thickness(Enum):
    REGULAR = 1
    MEDIUM = 2
    BOLD = 3

class ImageUtils:
    def __init__(self, image: np.ndarray) -> None:
        self.image = image
    @property
    def size(self):
        return self.image.shape
    def show(self) -> None:
        cv2.imshow('image window', self.image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    def save(self, file_path: str) -> None:
        cv2.imwrite(file_path, self.image)
        logging.info("Successfully write image to {}".format(file_path))

class Invitation:
    def __init__(
        self, 
        background_file: str,
        font_dir: str
    ) -> None:
        image = cv2.imread(background_file)
        self.background_image = image
        self.export_image = image
        self.image_size = self.background_image.shape
        self.fonts = {
            Thickness.REGULAR: os.path.join(font_dir, "SourceHanSerif-Regular.ttc"),
            Thickness.MEDIUM:  os.path.join(font_dir, "SourceHanSerif-Medium.ttc"),
            Thickness.BOLD:    os.path.join(font_dir, "SourceHanSerif-Bold.ttc"),
        }
        assert [os.path.isfile(path) for path in self.fonts.values()] \
            == [True] * len(self.fonts.values()), "Some fonts cannot be found."

    @property
    def size(self) -> tuple:
        """ (height, width, channel) """
        return self.image_size

    def clear(self):
        self.export_image = deepcopy(self.background_image)

    @classmethod
    def calculate_x_center(
        self,
        width: int,
        content: str,
        font_style: ImageFont.FreeTypeFont,
    ):
        bbox = font_style.getbbox(content)
        return (width - (bbox[0] + bbox[2])) // 2
        

    def put_text(
        self,
        content: str,
        font_size: int,
        thickness: Thickness,
        color: tuple = (222, 205, 137),
        distance_percentage_from_top: float = 0.10
    ) -> np.ndarray:
        height, width, _ = self.image_size
        image = Image.fromarray(cv2.cvtColor(self.export_image, cv2.COLOR_BGR2RGB))
        draw = ImageDraw.Draw(image)
        font_style = ImageFont.truetype(self.fonts[thickness], font_size, encoding="utf-8")
        draw.text((self.calculate_x_center(width, content, font_style), int(height * distance_percentage_from_top)), 
                  content, color, font=font_style)
        self.export_image = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)
        return ImageUtils(self.export_image)