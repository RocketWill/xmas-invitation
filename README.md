<!--
 * @Date: 2021-12-12 16:14:37
 * @LastEditors: will.chengyong
 * @LastEditTime: 2021-12-12 16:33:19
 * @FilePath: /general/christmas/README.md
-->
# Invitation Generator
### Pre requirements
- install packages: `pip install -r requirements.txt`

### Add Content
在 [content.py](./content.py) 填寫邀請函內容  
`create_content` 參數說明：  
1. `invitation_receiver`: 邀請卡收件人
2. `gift_sender`: 交換禮物對象

### Run Test
check [main.py](./main.py)  
```python
from invitation import Invitation
from content import create_content

# 創建邀請卡
invitation = Invitation(底圖路徑, 字體目錄)
# 創建內容
content = create_content(invitation_receiver="Willmermaid", gift_sender="玉澤演")
for c in content.values():
    image = invitation.put_text(
        c.get("text"),         # 文字內容
        c.get("font_size"),    # 字體大小
        c.get("thickness"),    # 字體粗細
        color=c.get("color"),  # 文字顏色
        distance_percentage_from_top=c.get("margin_top") # 距離圖片頂部距離（百分比）
    )

# 展示圖像
image.show()
# 保存圖像
image.save(圖片保存路徑)
# 清除所有文字
image.clear()
```

### Result
![result](./assets/xmas-invitation.jpg "Result image")

